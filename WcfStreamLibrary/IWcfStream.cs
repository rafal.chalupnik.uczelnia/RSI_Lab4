﻿using System.IO;
using System.ServiceModel;

namespace WcfStreamLibrary
{
    [ServiceContract]
    public interface IWcfStream
    {
        [OperationContract]
        Stream GetStream(string _name);

        [OperationContract]
        ResponseFileMessage GetMStream(RequestFileMessage _request);
    }

    [MessageContract]
    public class RequestFileMessage
    {
        [MessageBodyMember]
        public string Name { get; set; }
    }

    [MessageContract]
    public class ResponseFileMessage
    {
        [MessageHeader]
        public string Name { get; set; }
        [MessageHeader]
        public long Size { get; set; }
        [MessageBodyMember]
        public Stream Stream { get; set; }
    }
}
