﻿using System;
using System.IO;

namespace WcfStreamLibrary
{
    public class WcfStream : IWcfStream
    {
        public ResponseFileMessage GetMStream(RequestFileMessage _request)
        {
            Console.WriteLine($"Entered GetMStream({_request.Name})");

            try
            {
                var stream = File.OpenRead(@"image.jpg");
                return new ResponseFileMessage()
                {
                    Name = _request.Name,
                    Size = stream.Length,
                    Stream = stream
                };
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception occiured: {exception.Message}");
                Console.WriteLine($"Stack trace: {exception.StackTrace}");

                return null;
            }
        }

        public Stream GetStream(string _name)
        {
            Console.WriteLine($"Entered GetStream({_name})");

            try
            {
                return File.OpenRead(@"image.jpg");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception occiured: {exception.Message}");
                Console.WriteLine($"Stack trace: {exception.StackTrace}");

                return null;
            }
        }
    }
}
