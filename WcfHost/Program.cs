﻿using System;
using System.Linq;
using System.ServiceModel;
using CallbackCalculator;
using WcfLibrary;

namespace WcfHost
{
    public class Program
    {
        public static void Main(string[] _args)
        {
            var serviceHost = new ServiceHost(typeof(Service));
            var calculatorHost = new ServiceHost(typeof(Calculator));

            try
            {
                serviceHost.Open();
                Console.WriteLine("Service started!");
                calculatorHost.Open();
                Console.WriteLine("Calculator started!");
                Console.WriteLine("Press Enter to exit.");

                Console.ReadLine();
                serviceHost.Close();
                calculatorHost.Close();
            }
            catch (CommunicationException exception)
            {
                Console.WriteLine($"Exception occured: {exception.Message}");
                Console.WriteLine(exception.StackTrace);
                serviceHost.Abort();
                calculatorHost.Abort();
                Console.ReadLine();
            }
        }
    }
}
