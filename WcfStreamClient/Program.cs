﻿using System;
using System.IO;

namespace WcfStreamClient
{
    public class Program
    {
        public static void Main(string[] _args)
        {
            var client = new WcfStreamReference.WcfStreamClient();
            /*Console.WriteLine("Calling GetStream()");

            var receivedStream = client.GetStream("image.jpg");

            var fileStream = File.OpenWrite("image.jpg");
            receivedStream.CopyTo(fileStream);

            fileStream.Close();

            Console.WriteLine("File saved successfully!");
            */

            Console.WriteLine("Calling GetMStream()");
            var filePath = "image.jpg";
            client.GetMStream(ref filePath, out var receivedStream);

            var fileStream = File.OpenWrite("image.jpg");
            receivedStream.CopyTo(fileStream);

            fileStream.Close();

            Console.WriteLine("File saved successfully!");
        }
    }
}
