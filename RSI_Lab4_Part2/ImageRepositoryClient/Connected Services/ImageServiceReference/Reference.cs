﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ImageRepositoryClient.ImageServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ImageServiceReference.IImageRepository")]
    public interface IImageRepository {
        
        // CODEGEN: Generating message contract since the operation AddImage is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IImageRepository/AddImage", ReplyAction="http://tempuri.org/IImageRepository/AddImageResponse")]
        AddImageResponse AddImage(ImageDto request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IImageRepository/AddImage", ReplyAction="http://tempuri.org/IImageRepository/AddImageResponse")]
        System.Threading.Tasks.Task<AddImageResponse> AddImageAsync(ImageDto request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IImageRepository/DeleteImage", ReplyAction="http://tempuri.org/IImageRepository/DeleteImageResponse")]
        bool DeleteImage(int _index);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IImageRepository/DeleteImage", ReplyAction="http://tempuri.org/IImageRepository/DeleteImageResponse")]
        System.Threading.Tasks.Task<bool> DeleteImageAsync(int _index);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IImageRepository/GetAllImages", ReplyAction="http://tempuri.org/IImageRepository/GetAllImagesResponse")]
        string GetAllImages();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IImageRepository/GetAllImages", ReplyAction="http://tempuri.org/IImageRepository/GetAllImagesResponse")]
        System.Threading.Tasks.Task<string> GetAllImagesAsync();
        
        // CODEGEN: Generating message contract since the wrapper name (ImageRequest) of message ImageRequest does not match the default value (GetImage)
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IImageRepository/GetImage", ReplyAction="http://tempuri.org/IImageRepository/GetImageResponse")]
        ImageDto GetImage(ImageRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IImageRepository/GetImage", ReplyAction="http://tempuri.org/IImageRepository/GetImageResponse")]
        System.Threading.Tasks.Task<ImageDto> GetImageAsync(ImageRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ImageDto", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class ImageDto {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public string Description;
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public string FilePath;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public System.IO.Stream Stream;
        
        public ImageDto() {
        }
        
        public ImageDto(string Description, string FilePath, System.IO.Stream Stream) {
            this.Description = Description;
            this.FilePath = FilePath;
            this.Stream = Stream;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class AddImageResponse {
        
        public AddImageResponse() {
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ImageRequest", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class ImageRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public int Index;
        
        public ImageRequest() {
        }
        
        public ImageRequest(int Index) {
            this.Index = Index;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IImageRepositoryChannel : IImageRepository, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ImageRepositoryClient : System.ServiceModel.ClientBase<IImageRepository>, IImageRepository {
        
        public ImageRepositoryClient() {
        }
        
        public ImageRepositoryClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ImageRepositoryClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ImageRepositoryClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ImageRepositoryClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        AddImageResponse IImageRepository.AddImage(ImageDto request) {
            return base.Channel.AddImage(request);
        }
        
        public void AddImage(string Description, string FilePath, System.IO.Stream Stream) {
            ImageDto inValue = new ImageDto();
            inValue.Description = Description;
            inValue.FilePath = FilePath;
            inValue.Stream = Stream;
            AddImageResponse retVal = ((IImageRepository)(this)).AddImage(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<AddImageResponse> IImageRepository.AddImageAsync(ImageDto request) {
            return base.Channel.AddImageAsync(request);
        }
        
        public System.Threading.Tasks.Task<AddImageResponse> AddImageAsync(string Description, string FilePath, System.IO.Stream Stream) {
            ImageDto inValue = new ImageDto();
            inValue.Description = Description;
            inValue.FilePath = FilePath;
            inValue.Stream = Stream;
            return ((IImageRepository)(this)).AddImageAsync(inValue);
        }
        
        public bool DeleteImage(int _index) {
            return base.Channel.DeleteImage(_index);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteImageAsync(int _index) {
            return base.Channel.DeleteImageAsync(_index);
        }
        
        public string GetAllImages() {
            return base.Channel.GetAllImages();
        }
        
        public System.Threading.Tasks.Task<string> GetAllImagesAsync() {
            return base.Channel.GetAllImagesAsync();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        ImageDto IImageRepository.GetImage(ImageRequest request) {
            return base.Channel.GetImage(request);
        }
        
        public string GetImage(int Index, out string FilePath, out System.IO.Stream Stream) {
            ImageRequest inValue = new ImageRequest();
            inValue.Index = Index;
            ImageDto retVal = ((IImageRepository)(this)).GetImage(inValue);
            FilePath = retVal.FilePath;
            Stream = retVal.Stream;
            return retVal.Description;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<ImageDto> IImageRepository.GetImageAsync(ImageRequest request) {
            return base.Channel.GetImageAsync(request);
        }
        
        public System.Threading.Tasks.Task<ImageDto> GetImageAsync(int Index) {
            ImageRequest inValue = new ImageRequest();
            inValue.Index = Index;
            return ((IImageRepository)(this)).GetImageAsync(inValue);
        }
    }
}
