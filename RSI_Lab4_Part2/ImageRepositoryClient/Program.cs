﻿using System;
using System.IO;

namespace ImageRepositoryClient
{
    /// <summary>
    /// Klasa główna programu klienta usługi ImageRepository.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Referencja do klienta usługi.
        /// </summary>
        private static ImageServiceReference.ImageRepositoryClient imageRepositoryClient_;

        /// <summary>
        /// Dodanie obrazka do repozytorium.
        /// </summary>
        /// <param name="_command">Komenda wpisana przez użytkownika.</param>
        private static void AddImage(string _command)
        {
            var @params = _command.Split(' ');

            if (@params.Length != 3)
            {
                Console.WriteLine("Invalid parameters count.\n");
                return;
            }

            try
            {
                var fileStream = File.OpenRead(@params[1]);
                imageRepositoryClient_.AddImage(@params[2], @params[1], fileStream);
                fileStream.Close();

                Console.WriteLine("Command complete.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Usunięcie obrazka z repozytorium.
        /// </summary>
        /// <param name="_command">Komenda wpisana przez użytkownika.</param>
        private static void DeleteImage(string _command)
        {
            var @params = _command.Split(' ');

            if (@params.Length != 2)
            {
                Console.WriteLine("Invalid parameters count.\n");
                return;
            }

            var result = imageRepositoryClient_.DeleteImage(int.Parse(@params[1]));

            if (result)
                Console.WriteLine("File deleted successfully.\n");
            else
                Console.WriteLine("Could not delete file.\n");
        }

        /// <summary>
        /// Pobranie listy wszystkich obrazków z repozytorium.
        /// </summary>
        private static void GetAllImages()
        {
            var list = imageRepositoryClient_.GetAllImages();
            Console.WriteLine($"{list}\n");
        }

        /// <summary>
        /// Pobranie obrazka z repozytorium.
        /// </summary>
        /// <param name="_command">Komenda wpisana przez użytkownika.</param>
        private static void GetImage(string _command)
        {
            var @params = _command.Split(' ');

            if (@params.Length != 2)
            {
                Console.WriteLine("Invalid parameters count.\n");
                return;
            }

            imageRepositoryClient_.GetImage(int.Parse(@params[1]), out var filePath, out var stream);

            using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
                stream.Close();
            }

            Console.WriteLine("Image downloaded successfully!");
        }

        /// <summary>
        /// Wyświetlenie dostępnych komend.
        /// </summary>
        private static void List()
        {
            Console.WriteLine("");
            Console.WriteLine("Available commands:");
            Console.WriteLine("\taddImage <file_path> <description>");
            Console.WriteLine("\tdeleteImage <index>");
            Console.WriteLine("\tgetAllImages");
            Console.WriteLine("\tgetImage <index>");
            Console.WriteLine("\tlist");
            Console.WriteLine("");
        }

        /// <summary>
        /// Metoda główna klienta.
        /// </summary>
        /// <param name="_args">Argumenty linii komend, ignorowane.</param>
        public static void Main(string[] _args)
        {
            var command = "";
            imageRepositoryClient_ = new ImageServiceReference.ImageRepositoryClient();

            Console.WriteLine("Enter command (enter <list> for all commands:");
            while (!command.Contains("exit"))
            {
                command = Console.ReadLine();

                if (command.Contains("addImage"))
                    AddImage(command);
                else if (command.Contains("deleteImage"))
                    DeleteImage(command);
                else if (command.Contains("getAllImages"))
                    GetAllImages();
                else if (command.Contains("getImage"))
                    GetImage(command);
                else if (command.Contains("list"))
                    List();
            }
        }
    }
}
