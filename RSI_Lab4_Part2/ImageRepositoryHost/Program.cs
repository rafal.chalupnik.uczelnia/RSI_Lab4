﻿using System;
using System.ServiceModel;
using ImageRepositoryLibrary;

namespace ImageRepositoryHost
{
    /// <summary>
    /// Klasa programu hostującego usługę ImageRepository.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Metoda główna programu hostującego.
        /// </summary>
        /// <param name="_args">Argumenty linii komend, ignorowane.</param>
        public static void Main(string[] _args)
        {
            var serviceHost = new ServiceHost(typeof(ImageRepository));

            try
            {
                serviceHost.Open();
                Console.WriteLine("ImageRepositoryHost started!");
                Console.WriteLine("Press Enter to exit...");

                Console.ReadLine();
                serviceHost.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception occured: {exception.Message}");
                Console.WriteLine(exception.StackTrace);
                serviceHost.Abort();
                Console.ReadLine();
            }
        }
    }
}
