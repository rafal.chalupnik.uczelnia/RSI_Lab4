﻿using System.IO;
using System.ServiceModel;

namespace ImageRepositoryLibrary
{
    /// <summary>
    /// Kontrakt usługi ImageRepository.
    /// </summary>
    [ServiceContract]
    public interface IImageRepository
    {
        /// <summary>
        /// Dodaj obrazek do repozytorium.
        /// </summary>
        /// <param name="_image">Dane obrazka.</param>
        [OperationContract]
        void AddImage(ImageDto _image);

        /// <summary>
        /// Usuń obrazek o danym indeksie z repozytorium.
        /// </summary>
        /// <param name="_index">Indeks obrazka do usunięcia.</param>
        /// <returns>True, jeśli się udało; False w przeciwnym wypadku.</returns>
        [OperationContract]
        bool DeleteImage(int _index);

        /// <summary>
        /// Pobiera listę wszystkich obrazków w repozytorium.
        /// </summary>
        /// <returns>Lista wszystkich obrazków.</returns>
        [OperationContract]
        string GetAllImages();

        /// <summary>
        /// Pobiera obrazek o danym indeksie z repozytorium.
        /// </summary>
        /// <param name="_imageRequest">Źądanie pobrania obrazka.</param>
        /// <returns>Obrazek o danym indeksie.</returns>
        [OperationContract]
        ImageDto GetImage(ImageRequest _imageRequest);
    }

    /// <summary>
    /// Klasa reprezentująca obrazek.
    /// </summary>
    [MessageContract]
    public class ImageDto
    {
        /// <summary>
        /// Opis obrazka.
        /// </summary>
        [MessageHeader]
        public string Description { get; set; }

        /// <summary>
        /// Ścieżka pliku obrazka.
        /// </summary>
        [MessageHeader]
        public string FilePath { get; set; }

        /// <summary>
        /// Strumień obrazka.
        /// </summary>
        [MessageBodyMember]
        public Stream Stream { get; set; }
    }

    /// <summary>
    /// Klasa żądania obrazka.
    /// </summary>
    [MessageContract]
    public class ImageRequest
    {
        /// <summary>
        /// Indeks żądanego obrazka.
        /// </summary>
        [MessageHeader]
        public int Index { get; set; }
    }
}
