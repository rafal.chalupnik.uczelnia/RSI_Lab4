﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Text;

namespace ImageRepositoryLibrary
{
    /// <summary>
    /// Klasa realizująca kontrakt ImageRepository.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ImageRepository : IImageRepository
    {
        /// <summary>
        /// Lista opisów obrazków w repozytorium.
        /// </summary>
        private readonly List<string> descriptions_ = new List<string>();
        /// <summary>
        /// Lista ścieżek plików obrazków w repozytorium.
        /// </summary>
        private readonly List<string> filePaths_ = new List<string>();

        /// <inheritdoc/>
        public void AddImage(ImageDto _image)
        {
            Console.WriteLine("Entered AddImage.");

            if (_image == null)
            {
                Console.WriteLine("Exited AddImage - null parameter.");
                return;
            }

            if (string.IsNullOrEmpty(_image.Description)
                || string.IsNullOrEmpty(_image.FilePath)
                || _image.Stream == null)
            {
                Console.WriteLine("Exited AddImage - invalid ImageDto parameters.");
                return;
            }

            try
            {
                var filePath = Path.GetFileName(_image.FilePath);

                using (var fileStream = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write))
                {
                    _image.Stream.CopyTo(fileStream);
                }

                filePaths_.Add(filePath);
                descriptions_.Add(_image.Description);

                Console.WriteLine("AddImage ended successfully.");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception occured in AddImage: {exception.Message}");
            }
        }

        /// <inheritdoc/>
        public bool DeleteImage(int _index)
        {
            Console.WriteLine("Entered DeleteImage.");

            if (_index < 0 || _index >= filePaths_.Count)
            {
                Console.WriteLine($"Exited DeleteImage - invalid index: {_index}");
                return false;
            }

            try
            {
                File.Delete(filePaths_[_index]);
                filePaths_.RemoveAt(_index);
                descriptions_.RemoveAt(_index);

                Console.WriteLine("DeleteImage ended successfully.");
                return true;
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception occured in DeleteImage: {exception.Message}");
                return false;
            }
        }

        /// <inheritdoc/>
        public string GetAllImages()
        {
            Console.WriteLine("Entered GetAllImages.");

            if (filePaths_.Count == 0)
                return "Brak obrazków.";

            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Index:\tNazwa pliku:\tOpis:");

            for (var i = 0; i < filePaths_.Count; i++)
                stringBuilder.AppendLine($"{i}\t{filePaths_[i]}\t{descriptions_[i]}");

            Console.WriteLine("GetAllImages ended successfully.");
            return stringBuilder.ToString();
        }

        /// <inheritdoc/>
        public ImageDto GetImage(ImageRequest _imageRequest)
        {
            Console.WriteLine("Entered GetImage.");

            if (_imageRequest == null)
            {
                Console.WriteLine("Exited GetImage - null image request.");
                return null;
            }

            if (_imageRequest.Index < 0 || _imageRequest.Index >= filePaths_.Count)
            {
                Console.WriteLine("Exited GetImage - invalid index.");
                return null;
            }

            try
            {
                var filePath = filePaths_[_imageRequest.Index];
                var stream = new MemoryStream();
                using (var fileStream = File.OpenRead(filePath))
                {
                    fileStream.CopyTo(stream);
                }

                stream.Position = 0;

                Console.WriteLine("GetImage ended successfully.");
                return new ImageDto()
                {
                    Description = descriptions_[_imageRequest.Index],
                    FilePath = filePath,
                    Stream = stream
                };
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exited GetImage - exception occured: {exception.Message}");
                return null;
            }
        }
    }
}
