﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using WcfStreamLibrary;

namespace WcfStreamHost
{
    public class Program
    {
        public static void Main(string[] _args)
        {
            var baseAddress = new Uri("http://localhost:10000");
            var serviceHost = new ServiceHost(typeof(WcfStream), baseAddress);
            var binding = new BasicHttpBinding()
            {
                MaxBufferSize = 8192,
                MaxReceivedMessageSize = 1000000000,
                TransferMode = TransferMode.Streamed
            };
            serviceHost.AddServiceEndpoint(typeof(IWcfStream), binding, baseAddress);
            serviceHost.Description.Behaviors.Add(new ServiceMetadataBehavior {HttpGetEnabled = true});

            try
            {
                serviceHost.Open();
                Console.WriteLine("Service started!");
                Console.WriteLine("Press Enter to exit.");

                Console.ReadLine();
                serviceHost.Close();
            }
            catch (CommunicationException exception)
            {
                Console.WriteLine($"Exception occured: {exception.Message}");
                Console.WriteLine(exception.StackTrace);
                serviceHost.Abort();
                Console.ReadLine();
            }
        }
    }
}
