﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace CallbackCalculator
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(ICalculatorCallback))]
    public interface ICalculator
    {
        [OperationContract(IsOneWay = true)]
        void DoSomething(int _time);

        [OperationContract(IsOneWay = true)]
        void Silnia(int _n);
    }

    [ServiceContract]
    public interface ICalculatorCallback
    {
        [OperationContract(IsOneWay = true)]
        void ReturnDoSomething(string _result);

        [OperationContract(IsOneWay = true)]
        void ReturnSilnia(int _result);
    }
}
