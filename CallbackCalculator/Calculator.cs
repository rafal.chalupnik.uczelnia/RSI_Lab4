﻿using System;
using System.ServiceModel;
using System.Threading;

namespace CallbackCalculator
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class Calculator : ICalculator
    {
        private readonly ICalculatorCallback callback_ 
            = OperationContext.Current.GetCallbackChannel<ICalculatorCallback>();

        public void DoSomething(int _time)
        {
            Console.WriteLine($"Entered DoSomething with argument: {_time}");
            _time = Math.Max(1000, _time * 1000);
            Thread.Sleep(_time);
            callback_.ReturnDoSomething($"DoSomething was running for {_time}");
        }

        public void Silnia(int _n)
        {
            Console.WriteLine($"Entered Silnia with argument: {_n}");
            Thread.Sleep(1000);

            var result = 1;
            for (var i = 2; i < _n; i++)
                result *= i;

            callback_.ReturnSilnia(result);
        }
    }
}
