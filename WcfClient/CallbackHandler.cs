﻿using System;
using WcfClient.CalculatorReference;

namespace WcfClient
{
    public class CallbackHandler : ICalculatorCallback
    {
        public void ReturnDoSomething(string _result)
        {
            Console.WriteLine($"DoSomething completed with result: {_result}");
        }

        public void ReturnSilnia(int _result)
        {
            Console.WriteLine($"Silnia completed with result: {_result}");
        }
    }
}