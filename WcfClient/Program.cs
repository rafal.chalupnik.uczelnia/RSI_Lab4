﻿using System;
using System.ServiceModel;
using System.Threading;
using WcfClient.CalculatorReference;
using WcfClient.ServiceReference;

namespace WcfClient
{
    public class Program
    {
        private static void ServiceClient()
        {
            const string argument = "Client";
            var client = new ServiceClient();

            try
            {
                Console.WriteLine($"Enter Function1 with argument: {argument}");
                client.Function1(argument);
                Thread.Sleep(10);
                Console.WriteLine("Continuing after Function1");

                Console.WriteLine($"Enter Function2 with argument: {argument}");
                client.Function2(argument);
                Thread.Sleep(10);
                Console.WriteLine("Continuing after Function2");

                Console.WriteLine($"Enter Function1 with argument: {argument}");
                client.Function1(argument);
                Thread.Sleep(10);
                Console.WriteLine("Continuing after Function1");

                client.Close();
                Console.WriteLine("Client program complete. Press Enter to exit.");
                Console.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Exception occured: {exception.Message}");
                Console.WriteLine(exception.StackTrace);
                Console.ReadLine();
            }
        }

        private static void CalculatorClient()
        {
            var callbackHandler = new CallbackHandler();
            var instanceContext = new InstanceContext(callbackHandler);
            var calculatorClient = new CalculatorClient(instanceContext);

            var silniaValue = 10;
            Console.WriteLine($"Entered Silnia with argument: {silniaValue}");
            calculatorClient.Silnia(silniaValue);
            silniaValue = 20;
            Console.WriteLine($"Entered Silnia with argument: {silniaValue}");
            calculatorClient.Silnia(silniaValue);

            var doSomethingValue = 2;
            Console.WriteLine($"Entered DoSomething with argument: {doSomethingValue}");
            calculatorClient.DoSomething(doSomethingValue);

            Console.WriteLine("Waiting for results...");
            Thread.Sleep(5000);

            calculatorClient.Close();
            Console.WriteLine("Client program complete. Press Enter to exit.");
            Console.ReadLine();
        }

        public static void Main(string[] _args)
        {
            // ServiceClient();
            CalculatorClient();
        }
    }
}
