﻿using System;
using System.ServiceModel;
using System.Threading;

namespace WcfLibrary
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class Service : IService
    {
        public void Function1(string _string)
        {
            Console.WriteLine($"Enter Function1 with argument: {_string}");
            Thread.Sleep(2000);
            Console.WriteLine($"Exit Function1 with argument: {_string}");
        }

        public void Function2(string _string)
        {
            Console.WriteLine($"Enter Function2 with argument: {_string}");
            Thread.Sleep(2000);
            Console.WriteLine($"Exit Function2 with argument: {_string}");
        }
    }
}
