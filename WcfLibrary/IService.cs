﻿using System;
using System.ServiceModel;

namespace WcfLibrary
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        void Function1(string _string);

        [OperationContract(IsOneWay = true)]
        void Function2(string _string);
    }
}
